<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'include/meta.php';?>

    <title>Facility</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <?php include 'include/css.php';?>
</head>
<body class="counter-scroll header_sticky">
    <div class="wrapper">
        <div id="page">
            <?php include 'include/header.php';?>

            <div id="header-baner">
                <div class="container">
                    <div class="text-banner wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                        <div class="baner-tittle">
                            Facilities
                        </div>
                    </div>
                </div>
            </div>
            <div id="main-content" class="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" class="site-content clearfix">
                        <div id="inner-content" class="inner-content-wrap">
                            <div class="page-content">
                                <section class="fl-row intro-middle">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="70" data-smobile="70" style="height: 120px;"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro01.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Euro Surgery</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="themesflat-spacer clearfix" data-desktop="8" data-mobile="8" data-smobile="8" style="height: 8px;"></div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="100ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro04.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Neuro Surgery</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="themesflat-spacer clearfix" data-desktop="8" data-mobile="8" data-smobile="8" style="height: 8px;"></div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro05.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Orthopedic Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="themesflat-spacer clearfix" data-desktop="8" data-mobile="8" data-smobile="8" style="height: 8px;"></div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro06.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Gynec Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="100ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro07.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Surgery Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro08.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Dental Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro08.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Physician Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro08.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Pulmo Physician Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro08.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Pediatric Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                                                    <div class="img">
                                                        <img src="img/intro08.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <a href="contact">Cardiothoracic & Vascular Surgery Unit</a>
                                                        </div>
                                                        <div class="flat-read-more">
                                                            <a href="contact" class="themesflat-button font-default small">
                                                                <span>Contact Now<i class="fa fa-arrow-right"></i> </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="80" data-mobile="30" data-smobile="30" style="height: 80px;"></div>
                                        </div>
                                    </div>
                                </section>

                                <section class="services-faq">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="130" data-mobile="70" data-smobile="70" style="height: 130px;"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="box-services-faq wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                    <div class="box-wrap-services-faq">
                                                        <div class="feature-box-services-fqa">
                                                            <img src="img/Group962.webp" alt="image" />
                                                        </div>
                                                        <div class="content-services-faq">
                                                            <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                            <div class="title-heading">
                                                                FAQ
                                                            </div>
                                                            <p>Lorem Ipsum is simply dummy text of available in market</p>
                                                            <div class="themesflat-spacer clearfix" data-desktop="50" data-mobile="50" data-smobile="50" style="height: 50px;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="themesflat-spacer clearfix" data-desktop="63" data-mobile="50" data-smobile="50" style="height: 63px;"></div>
                                                    <div class="box-faq">
                                                        <!-- <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28" style="height: 28px;"></div>
                                                                <h4><span>01.</span><span class="pdl-12">What does you do?</span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="17" data-mobile="17" data-smobile="17" style="height: 17px;"></div>
                                                            </div>
                                                            <div class="content-item-faq" style="display: none;">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    Lorem Ipsum is simply dummy of free available in market the printing and typesetting industry has been industry's standard.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div> -->
                                                        <!-- <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="26" data-mobile="26" data-smobile="26" style="height: 26px;"></div>
                                                                <h4><span>02.</span><span class="pdl-12">What is graphics design?</span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="16" data-mobile="16" data-smobile="16" style="height: 16px;"></div>
                                                            </div>
                                                            <div class="content-item-faq" style="display: none;">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    Lorem Ipsum is simply dummy of free available in market the printing and typesetting industry has been industry's standard.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28" style="height: 28px;"></div>
                                                                <h4><span>03.</span><span class="pdl-12">Why we are the best?</span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15" style="height: 15px;"></div>
                                                            </div>
                                                            <div class="content-item-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    Lorem Ipsum is simply dummy of free available in market the printing and typesetting industry has been industry's standard.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="box-services-faq wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                    <div class="themesflat-spacer clearfix" data-desktop="30" data-mobile="30" data-smobile="30" style="height: 30px;"></div>
                                                    <div class="box-faq">
                                                        <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="27" data-mobile="27" data-smobile="27" style="height: 27px;"></div>
                                                                <h4><span>01.</span><span class="pdl-12">What kind of services available at Soul Healthcare?</span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="16" data-mobile="16" data-smobile="16" style="height: 16px;"></div>
                                                            </div>
                                                            <div class="content-item-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    Soul Healthcare offers Consultations for children, women, men and senior citizens.We also offer Lab Tests, Health Checks, Vaccinations, Dental services and Diabetes care. We have all facilities & medical equipments to perform anykind of Operations.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28" style="height: 28px;"></div>
                                                                <h4><span>02.</span><span class="pdl-12">What is Soul's working hours?</span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15" style="height: 15px;"></div>
                                                            </div>
                                                            <div class="content-item-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    We are open 24x7 to serve you. Please contact us on 565689569.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="27" data-mobile="27" data-smobile="27" style="height: 27px;"></div>
                                                                <h4><span>03.</span><span class="pdl-12">Are you open on Sundays? </span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="16" data-mobile="16" data-smobile="16" style="height: 16px;"></div>
                                                            </div>
                                                            <div class="content-item-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    Yes, We are. Even on holidays & festivals, We are open 24x7.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28" style="height: 28px;"></div>
                                                                <h4><span>04.</span><span class="pdl-12">Are all the Doctors available 24x7? </span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15" style="height: 15px;"></div>
                                                            </div>
                                                            <div class="content-item-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                    Doctors are available only on pre-determined consultation hours. Please contact with us on 56569989
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="item-faq">
                                                            <div class="title-heading-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28" style="height: 28px;"></div>
                                                                <h4><span>05.</span><span class="pdl-12">Soul is taking Emergency Tratments?</span></h4>
                                                                <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15" style="height: 15px;"></div>
                                                            </div>
                                                            <div class="content-item-faq">
                                                                <div class="themesflat-spacer clearfix" data-desktop="24" data-mobile="24" data-smobile="24" style="height: 24px;"></div>
                                                                <p>
                                                                   Yes, We are into healthcare . We have Operation theater & all the reqired equipments, Where can we attend to minor cuts, bruises, sores etc. with our well experienced doctors & staff.
                                                                </p>
                                                                <div class="themesflat-spacer clearfix" data-desktop="25" data-mobile="25" data-smobile="25" style="height: 25px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="233" data-mobile="170" data-smobile="80" style="height: 233px;"></div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'include/footer.php';?>
        </div>
    </div>

    <?php include 'include/js.php';?>
</body>
</html>
