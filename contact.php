<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'include/meta.php';?>

    <title>Contact</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <?php include 'include/css.php';?>
</head>
<body class="counter-scroll header_sticky">
    <div class="wrapper">
        <div id="page">
            <?php include 'include/header.php';?>

            <div id="header-baner">
                <div class="container">
                    <div class="text-banner wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                        <div class="baner-tittle">
                            Contact Us
                        </div>
                    </div>
                </div>
            </div>

            <div id="main-content" class="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" class="site-content clearfix">
                        <div id="inner-content" class="inner-content-wrap">
                            <div class="page-content">
                                <section class="contact">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="70" data-smobile="70" style="height: 120px;"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="box-contact-top">
                                                    <div class="box-wrap-contact wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                                                        <h3 class="heading">
                                                            GET IN TOUCH
                                                        </h3>
                                                        <div class="themesflat-spacer clearfix" data-desktop="12" data-mobile="12" data-smobile="12" style="height: 12px;"></div>
                                                        <div class="title-heading text-color-title-sidebar">
                                                            We want to share our<br />
                                                            location to find us easily.
                                                        </div>
                                                    </div>
                                                    <div class="box-icon-contact">
                                                        <div class="fl-icon-box wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                                                            <div class="themesflat-spacer clearfix" data-desktop="90" data-mobile="90" data-smobile="90" style="height: 90px;"></div>
                                                            <div class="icon-wrap">
                                                                <i class="icon-icon11"></i>
                                                            </div>
                                                            <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35" style="height: 35px;"></div>
                                                            <h4 class="text-color-title-sidebar">Our Address</h4>
                                                            <div class="themesflat-spacer clearfix" data-desktop="8" data-mobile="8" data-smobile="8" style="height: 8px;"></div>
                                                            <ul>
                                                                <li class="sub-box">
                                                                    Opp. Madhavdeep, Kalubha Road, Kalanala, Bhavnagar - 364002
                                                                </li>
                                                            </ul>
                                                            <div class="themesflat-spacer clearfix" data-desktop="85" data-mobile="85" data-smobile="0" style="height: 85px;"></div>
                                                        </div>
                                                        <div class="fl-icon-box wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                                                            <div class="themesflat-spacer clearfix" data-desktop="90" data-mobile="90" data-smobile="90" style="height: 90px;"></div>
                                                            <div class="icon-wrap">
                                                                <i class="icon-icon13"></i>
                                                            </div>
                                                            <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35" style="height: 35px;"></div>
                                                            <h4 class="text-color-title-sidebar">Our Phone</h4>
                                                            <div class="themesflat-spacer clearfix" data-desktop="8" data-mobile="8" data-smobile="8" style="height: 8px;"></div>
                                                            <ul>
                                                                <li class="sub-box">
                                                                    Mobile No : +91 9073 98 98 98
                                                                </li>
                                                            </ul>
                                                            <div class="themesflat-spacer clearfix" data-desktop="85" data-mobile="85" data-smobile="0" style="height: 85px;"></div>
                                                        </div>
                                                        <div class="fl-icon-box wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                                                            <div class="themesflat-spacer clearfix" data-desktop="90" data-mobile="90" data-smobile="90" style="height: 90px;"></div>
                                                            <div class="icon-wrap">
                                                                <i class="icon-icon12 sz-34"></i>
                                                            </div>
                                                            <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35" style="height: 35px;"></div>
                                                            <h4 class="text-color-title-sidebar">Our Email</h4>
                                                            <div class="themesflat-spacer clearfix" data-desktop="8" data-mobile="8" data-smobile="8" style="height: 8px;"></div>
                                                            <ul>
                                                                <li class="sub-box">
                                                                    Email : soulhealthcares@gmail.com
                                                                </li>
                                                            </ul>
                                                            <div class="themesflat-spacer clearfix" data-desktop="85" data-mobile="85" data-smobile="85" style="height: 85px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="280" data-mobile="280" data-smobile="280" style="height: 280px;"></div>
                                        </div>
                                    </div>
                                </section>
                                <div class="row">
                                    <div class="themesflat-spacer clearfix" data-desktop="280" data-mobile="280" data-smobile="280" style="height: 280px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'include/footer.php';?>
        </div>
    </div>

    <?php include 'include/js.php';?>
</body>
</html>
