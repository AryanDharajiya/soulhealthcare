<header class="header clearfix">
    <div id="header">
        <div id="site-header">
            <div class="logo1">
                <a href="/">
                    <img src="img/logo.png" alt="image"/>
                </a>
            </div>

            <div class="nav-wrap">
                <nav id="mainnav" class="mainnav">
                    <ul class="menu">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="facility">Facility</a>
                        </li>
                        <li>
                            <a href="about">About Us</a>
                        </li>                        
                        <li>
                            <a href="contact">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="contact">
                <div class="top-bar-right">
                    <div class="call-us">
                        <div class="icon-call-us"></div>
                        <div class="content-call-us">
                            <p>Call Us</p>
                            <a href="tel:+91 9073989898" class="font-bold text-color-title-sidebar">+91 9073 98 98 98</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-menu">
                <span></span>
            </div>
        </div>
    </div>
</header>