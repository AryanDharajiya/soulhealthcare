<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery-validate.js"></script>
<script src="js/main.js"></script>

<script src="js/rev-slider.js"></script>

<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/slider_v1.js"></script>
<script src="js/slider_v2.js"></script>

<script src="js/revolution.extension.actions.min.js"></script>
<script src="js/revolution.extension.carousel.min.js"></script>
<script src="js/revolution.extension.kenburn.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.migration.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/animation.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/carousel.js"></script>