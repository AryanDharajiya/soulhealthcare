<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="themesflat-spacer clearfix" data-desktop="95" data-mobile="95" data-smobile="25"></div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="box-list-footer">
                    <div class="box-list box-content">
                        <div class="logo-footer">   
                            <a href="/">
                                <img src="img/logowhite.png" alt="image" />
                            </a>
                        </div>
                        <div class="fcont">
                            <p>Soul Healthcare is a Multi Speciality Hospital. This world class & highly sophisticated medical establishment offers all major medical specialities, subspecialities, investigation & diagnostics facility, rehabilitation & physical therapy care under one roof.</p>
                        </div>
                    </div>
                    <div class="box-list box-useful">
                        <div class="title-footer text-color-white"><a>Quick Link</a></div>
                        <ul class="one-half first">
                            <li>
                                <a href="/" title="">
                                    <span><i class="fa fa-square"></i></span>Home
                                </a>
                            </li>
                            <li>
                                <a href="facility" title="">
                                    <span><i class="fa fa-square"></i></span>Facility
                                </a>
                            </li>
                            <li>
                                <a href="about" title="">
                                    <span><i class="fa fa-square"></i></span>About Us
                                </a>
                            </li>
                            <li>
                                <a href="contact" title="">
                                    <span><i class="fa fa-square"></i></span>Contact Us
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="box-list box-contact">
                        <div class="title-footer text-color-white">Get in Touch</div>
                        <ul class="one-half first">
                            <li>
                                <span><i class="fa fa-map-marker flat-icon-footer"></i></span>
                                <span>Opp. Madhavdeep, Kalubha Road, Kalanala, Bhavnagar - 364002</span>
                            </li>
                            <li>
                                <span><i class="fa fa-phone flat-icon-footer"></i></span>
                                <span><a href="+91 9073989898">+91 9073 98 98 98</a></span>
                            </li>
                            <li>
                                <span><i class="fa fa-envelope flat-icon-footer"></i></span>
                                <span>
                                    <a href="mailto:soulhealthcares@gmail.com">
                                        <span class="__cf_email__" data-cfemail="mailto:soulhealthcares@gmail.com">soulhealthcares@gmail.com</span>
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="themesflat-spacer clearfix" data-desktop="95" data-mobile="50" data-smobile="40"></div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">                
                <h3 class="copyright">
                    Copyright © 2021 Soulhealthcares
                </h3>
                <ul class="list-icon-bottom">
                    <li class="bottom-icon-active">
                        <a href="https://www.facebook.com/soulhealthcare" title=""><i class="fa fa-facebook-f"></i></a>
                    </li>
                    <li class="bottom-icon-style">
                        <a href="#" title=""><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="bottom-icon-style">
                        <a href="https://www.instagram.com/soul.healthcare/" title=""><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="bottom-icon-style">
                        <a href="#" title=""><i class="fa fa-linkedin-in"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="button-go-top">
    <a href="#" title="" class="go-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>