<!DOCTYPE html>
<html lang="en">
<head>

    <?php include 'include/meta.php';?> 

    <title></title>    
    <meta name="description" content="">
    <meta name="keywords" content="">

    <?php include 'include/css.php';?> 

</head>
<body class="counter-scroll header_sticky">
    <div class="wrapper">
        <div id="page">
            
            <?php include 'include/header.php';?>

            <div id="main-content" class="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" class="site-content clearfix">
                        <div id="inner-content" class="inner-content-wrap">
                            <div class="page-content">
                                <div class="rev_slider_wrapper fullwidthbanner-container">
                                    <div id="rev-slider2" class="rev_slider fullwidthabanner">
                                        <ul>
                                            <li data-transition="random">
                                                <img src="img/soul-web-banner.jpg" alt="image" data-bgposition="center center" data-no-retina />

                                                <div
                                                    class="tp-caption tp-resizeme slide-font-1"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['4','10','20','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['-160','-157','-157','-85']"
                                                    data-fontsize="['16']"
                                                    data-lineheight="['70','70','40','24']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="700"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    WE USE LATEST MEDICAL TECHNOLOGY
                                                </div>
                                                <div
                                                    class="tp-caption tp-resizeme slide-font-2"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['5','5','15','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['-80','-80','-80','-35']"
                                                    data-fontsize="['68','68','68','45']"
                                                    data-lineheight="['85','85','85','80']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    Let Us Brighten
                                                </div>
                                                <div
                                                    class="tp-caption tp-resizeme slide-font-2"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['5','9','19','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['4','2','2','15']"
                                                    data-fontsize="['68','68','68','45']"
                                                    data-lineheight="['65','85','85','80']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    Your Smile
                                                </div>
                                                <div
                                                    class="tp-caption"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['0','10','20','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['108','108','108','85']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    <a href="about-us.html" class="themesflat-button bg-accent big"><span>Who We Are</span></a>
                                                </div>
                                            </li>

                                            <li data-transition="random">
                                                <img src="img/soul-web-banner.jpg" alt="image" data-bgposition="center center" data-no-retina />

                                                <div
                                                    class="tp-caption tp-resizeme slide-font-1"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['4','10','20','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['-160','-157','-157','-85']"
                                                    data-fontsize="['16']"
                                                    data-lineheight="['70','70','40','24']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="700"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    WE USE LATEST MEDICAL TECHNOLOGY
                                                </div>
                                                <div
                                                    class="tp-caption tp-resizeme slide-font-2"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['5','5','15','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['-80','-80','-80','-35']"
                                                    data-fontsize="['68','68','68','45']"
                                                    data-lineheight="['85','85','85','80']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    Let Us Brighten
                                                </div>
                                                <div
                                                    class="tp-caption tp-resizeme slide-font-2"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['5','9','19','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['4','2','2','15']"
                                                    data-fontsize="['68','68','68','45']"
                                                    data-lineheight="['65','85','85','80']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    Your Smile
                                                </div>
                                                <div
                                                    class="tp-caption"
                                                    data-x="['left','left','left','center']"
                                                    data-hoffset="['0','10','20','-15']"
                                                    data-y="['middle','middle','middle','middle']"
                                                    data-voffset="['108','108','108','85']"
                                                    data-width="full"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                    data-mask_in="x:0px;y:[100%];"
                                                    data-mask_out="x:inherit;y:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                >
                                                    <a href="about-us.html" class="themesflat-button bg-accent big"><span>Who We Are</span></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <section class="fl-row intro-top">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="box-intro-top">
                                                    <div class="box-features-intro">
                                                        <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                        <div class="features-intro-top wow fadeInDown">
                                                            <img src="img/section-2.jpg" alt="image" />
                                                            <!-- <div class="features-inner">
                                                                <img src="img/Path18193.png" alt="image" />
                                                            </div> -->
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="20" data-mobile="20" data-smobile="20"></div>
                                                    </div>
                                                    <div class="box-content-intro">
                                                        <div class="themesflat-spacer clearfix" data-desktop="87" data-mobile="70" data-smobile="50"></div>
                                                        <h3 class="heading">
                                                            WHO WE ARE
                                                        </h3>
                                                        <div class="title-heading wow fadeInUp">
                                                            Why Choose<br />
                                                            Soul Health Cares
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15"></div>
                                                        <div class="row-content-intro wow fadeInUp">
                                                            <div class="cl-content-intro">
                                                                <div class="box-icon">
                                                                    <i class="icon-Path-18154 font-size-big"></i>
                                                                </div>
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28"></div>
                                                                <h3><a href="facility">Quality of Treatment </a></h3>
                                                                <p>
                                                                    Quality of treatment is paramount for anyone while choosing the list of the best Super Speciality Hospital in Bhavnagar.
                                                                </p>
                                                            </div>
                                                            <div class="cl-content-intro">
                                                                <div class="box-icon">
                                                                    <i class="icon-Path-18158 font-size-big"></i>
                                                                </div>
                                                                <div class="themesflat-spacer clearfix" data-desktop="28" data-mobile="28" data-smobile="28"></div>
                                                                <h3><a href="facility">Qualified Staff</a></h3>
                                                                <p>
                                                                    This Hospital is equipped with latest and advanced equipments and a competent and a team of well qualified and highly committed professionals.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="45" data-mobile="45" data-smobile="20"></div>                                                       
                                                        <div class="themesflat-spacer clearfix" data-desktop="78" data-mobile="70" data-smobile="50"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="fl-row intro-middle">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="150" data-mobile="70" data-smobile="70"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="box-heading-intro">
                                                    <h3 class="heading wow fadeInDown">
                                                        WHAT WE DO
                                                    </h3>
                                                    <div class="title-heading wow fadeInDown">
                                                        Providing Medical Care For The <br />
                                                        Sickest In Our Community.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="box-item wow fadeInUp">
                                                    <div class="img">
                                                        <img src="img/icu-on-wheels.jpg" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <h3>Ambulance Service</h3>
                                                        </div>
                                                        <p class="pdt-10 text-color-title-sidebar">
                                                            We are 24x7 responding to emergency number & Providing medical help to patients in any kind of injuries or illness as quick as possible.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box-item wow fadeInUp" data-wow-delay="100ms">
                                                    <div class="img">
                                                        <img src="img/lab-facility.jpg" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <h3>Healing Pathology Lab</h3>
                                                        </div>
                                                        <p class="pdt-10 text-color-title-sidebar" style="padding-bottom: 25px;">
                                                            A leading pathology lab of Bhavnagar with Microbiologists, Biochemists and specialists with doctorate degrees.

                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box-item wow fadeInUp" data-wow-delay="200ms">
                                                    <div class="img">
                                                        <img src="img/medical-store.jpg" alt="image" />
                                                    </div>
                                                    <div class="box-wrap">
                                                        <div class="title">
                                                            <h3>Zorba Pharmacy</h3>
                                                        </div>
                                                        <p class="pdt-10 text-color-title-sidebar" style="padding-bottom: 25px;">
                                                            A pharmacy with a range of products in the personal care, baby care, health and nutrition, wellness, and lifestyle categories.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="80" data-mobile="50" data-smobile="30"></div>
                                        </div>
                                    </div>
                                </section>

                                <section class="fl-row intro-bot">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="box-wrap-title wow fadeInDown">
                                                    <h3 class="heading text-color-white">
                                                        WHAT WE DO
                                                    </h3>
                                                    <div class="title-heading text-color-white">
                                                        What You Can Do<br />
                                                        with Soul Health Cares
                                                    </div>
                                                    <p class="text-color-white">hospital, an institution that is built, staffed, and equipped for the diagnosis of disease; for the treatment, both medical and surgical</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="box-features-bot">
                                                    <div class="themesflat-spacer clearfix" data-desktop="40" data-mobile="40" data-smobile="40"></div>
                                                    <img class="flat-img-intro" src="img/MaskGroup10.png" alt="image" />
                                                    <div class="features-bot-inner">
                                                        <img src="img/Ellipse989.png" alt="image" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="fl-row features">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="box-features wow fadeInUp">
                                                    <div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="40"></div>
                                                    <div class="col-box">
                                                        <div class="box-icon">
                                                            <img src="img/critical.svg">
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="38" data-mobile="38" data-smobile="38"></div>
                                                        <div class="box-wrap">
                                                            <h2 class="box-change font-rubik">
                                                                <h3>
                                                                    Critical Care
                                                                </h3>
                                                                <div class="cei">
                                                                    <img src="img/critical.png" width="125">
                                                                </div>
                                                            </h2>
                                                            <p>We have the most modern and specialized ICU infrastructure and world-class doctors to extend the best support and treatment.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-box">
                                                        <div class="box-icon active">
                                                            <img src="img/opd.svg">
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="38" data-mobile="38" data-smobile="38"></div>
                                                        <div class="box-wrap">
                                                            <h2 class="box-change font-rubik">
                                                                <h3>
                                                                    OPD
                                                                </h3>
                                                            </h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at est id leo</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-box">
                                                        <div class="box-icon">
                                                            <img src="img/operative.svg">
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="38" data-mobile="38" data-smobile="38"></div>
                                                        <div class="box-wrap">
                                                            <h2 class="box-change font-rubik">
                                                                <h3>
                                                                    Operative
                                                                </h3>
                                                            </h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at est id leo</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-box bd-none">
                                                        <div class="box-icon icon-18158">
                                                            <img src="img/super-sp.svg">
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="38" data-mobile="38" data-smobile="38"></div>
                                                        <div class="box-wrap">
                                                            <h2 class="box-change font-rubik">
                                                                <h3>
                                                                    Super Speciality Center
                                                                </h3>
                                                            </h2>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at est id leo</p>
                                                        </div>
                                                    </div>
                                                    <div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="30"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="fl-row our-team">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="box-heading-ot wow fadeInDown">
                                                    <h3 class="heading text-color-title-sidebar">
                                                        MEET OUR TEAM
                                                    </h3>
                                                    <div class="title-heading text-color-title-sidebar">
                                                        Meet Our Doctors
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/dilip-hadiyal.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Dilip R Hadiyal</h3>
                                                        <h4>MBBS, MD(Medicine)</h4>
                                                        <p>Physician Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/parthraj-umat.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Parthraj D Umat</h3>
                                                        <h4>MBBS, MD(E.Medicine)</h4>
                                                        <p>Physician Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/jainam-navadiya.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Jainam Navadiya</h3>
                                                        <h4>MBBS, DTCD, DNB</h4>
                                                        <p>Pulmo-Physician Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/nishad-gogdani.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Nishad P Gogdani</h3>
                                                        <h4>MBBS, MD</h4>
                                                        <p>Pulmo-Physician Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/nisarg-shah.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Nisarg P Shah</h3>
                                                        <h4>MBBS, DNB, FIAS(Ortho)</h4>
                                                        <p>Orthopedic Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/parth-sachapara.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Parth V Sachapara</h3>
                                                        <h4>MBBS, MS(Ortho)</h4>
                                                        <p>Orthopedic Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/tanmay-trivedi.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Tanmay Trivedi</h3>
                                                        <h4>MBBS, DNB(Neuro)</h4>
                                                        <p>Neuro Physician Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/arvind-valiya.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Arvind Valiya</h3>
                                                        <h4>MBBS, DNB(Urology)</h4>
                                                        <p>URO Surgery Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/medha-kanani.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Medha M Kanani</h3>
                                                        <h4>MBBS, MS(OBS & Gynec)</h4>
                                                        <p>Obstetric & Gynec Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/jayvir-parmar.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Jayvir Parmar</h3>
                                                        <h4>MBBS, MS(OBS & Gynec)</h4>
                                                        <p>Obstetric & Gynec Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/jayesh-shukla.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Jayesh B Shukla</h3>
                                                        <h4>MBBS, MS</h4>
                                                        <p>Surgery Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/dharati-patel.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Dharti Patel</h3>
                                                        <h4>BDS(Cosmetic Surgeon)</h4>
                                                        <p>Dental Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/anish-shukla.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Anish G Shukla</h3>
                                                        <h4>BDS</h4>
                                                        <p>Dental Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="dt">
                                                    <div class="dt-img">
                                                        <img src="img/doctor/hardev.jpg">
                                                    </div>
                                                    <div class="dt-cont">
                                                        <h3>Dr. Hardevsing R Mori</h3>
                                                        <h4>MBBS, MD(Paediatrics)</h4>
                                                        <p>Neonte & Paediatrics Unit</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="60" data-smobile="20"></div>
                                        </div>
                                    </div>
                                </section>

                                <section class="fl-row portfolio">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="60" data-smobile="50"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                                <div class="wrap-portfolio wow fadeInDown">
                                                    <h3 class="heading">
                                                        OUR TOP 5
                                                    </h3>
                                                    <div class="title-heading text-color-title-sidebar">
                                                        SPECIALITIES
                                                    </div>
                                                    <p class="text-color-title-sidebar">
                                                        Soul Healthcare has a unique combination of best in class technology, put to use by the best names in the medical profession; ensuring world-class health care for all patients. 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                                <div class="box-portfolio wow fadeInUp pdbt-30">
                                                    <img src="img/section-6-1.jpg" alt="image" />
                                                    <div class="overlay active">
                                                        <h4 class="text-color-title-sidebar">
                                                            <a href="facility">
                                                                ICU Unit
                                                            </a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                                <div class="box-portfolio wow fadeInUp pdbt-30">
                                                    <img src="img/section-6-2.jpg" alt="image" />
                                                    <div class="overlay">
                                                        <h4 class="text-color-title-sidebar">
                                                            <a href="facility">
                                                                Modular OT
                                                            </a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                                <div class="box-portfolio wow fadeInUp pdbt-30" data-wow-delay="200ms">
                                                    <img src="img/section-6-3.jpg" alt="image" />
                                                    <div class="overlay">
                                                        <h4 class="text-color-title-sidebar">
                                                            <a href="facility">
                                                                Troma Center
                                                            </a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                                <div class="box-portfolio wow fadeInUp pdbt-30" data-wow-delay="200ms">
                                                    <img src="img/section-6-4.jpg" alt="image" />
                                                    <div class="overlay">
                                                        <h4 class="text-color-title-sidebar">
                                                            <a href="facility">
                                                                Dialysis
                                                            </a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                                <div class="box-portfolio wow fadeInUp pdbt-30" data-wow-delay="400ms">
                                                    <img src="img/section-6-5.jpg" alt="image" />
                                                    <div class="overlay">
                                                        <h4 class="text-color-title-sidebar">
                                                            <a href="facility">
                                                                NICU
                                                            </a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="90" data-mobile="40" data-smobile="20"></div>
                                        </div>
                                    </div>
                                </section>

                                <section class="fl-row counter">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="70" data-smobile="70"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6 col-sm-12">
                                                <div class="col-counter fl-h1">
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                    <div class="box">
                                                        <div class="box-icon active">
                                                            <i class="icon-Path-18189 font-size-big"></i>
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15"></div>
                                                        <div class="title-heading text-color-white"><span class="number" data-from="0" data-to="34" data-speed="3000" data-inviewport="yes">34</span><span>+</span></div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="5" data-mobile="5" data-smobile="5"></div>
                                                        <h4 class="text-color-white font-raguler">Awards Win</h4>
                                                    </div>
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-12">
                                                <div class="col-counter fl-h1">
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                    <div class="box">
                                                        <div class="box-icon active">
                                                            <i class="icon-Path-18146 font-size-big"></i>
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15"></div>
                                                        <div class="title-heading text-color-white"><span class="number" data-from="0" data-to="95" data-speed="3000" data-inviewport="yes">95</span>K</div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="5" data-mobile="5" data-smobile="5"></div>
                                                        <h4 class="text-color-white font-raguler">Happy Customers</h4>
                                                    </div>
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-12">
                                                <div class="col-counter fl-h1">
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                    <div class="box">
                                                        <div class="box-icon active">
                                                            <i class="icon-Path-18167 font-size-big"></i>
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15"></div>
                                                        <div class="title-heading text-color-white"><span class="number" data-from="0" data-to="100" data-speed="3000" data-inviewport="yes">100</span>%</div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="5" data-mobile="5" data-smobile="5"></div>
                                                        <h4 class="text-color-white font-raguler pdl-5">Satisfaction</h4>
                                                    </div>
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-12">
                                                <div class="col-counter fl-h1">
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                    <div class="box">
                                                        <div class="box-icon active">
                                                            <i class="icon-Path-18145 font-size-big"></i>
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15"></div>
                                                        <div class="title-heading text-color-white"><span class="number" data-from="0" data-to="10" data-speed="3000" data-inviewport="yes">10</span>+</div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="5" data-mobile="5" data-smobile="5"></div>
                                                        <h4 class="text-color-white font-raguler">Years of experience</h4>
                                                    </div>
                                                    <div class="themesflat-spacer clearfix" data-desktop="35" data-mobile="35" data-smobile="35"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="91" data-mobile="50" data-smobile="50"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="row-counter">
                                                    <div class="box-counter-img">
                                                        <img src="img/section-7-2.jpg" alt="image" />
                                                    </div>
                                                    <div class="box-counter-wrap">
                                                        <div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="30"></div>
                                                        <div class="wrap-counter">
                                                            <div class="title-heading text-color-white">
                                                                True Healthcare For<br />
                                                                Your Family
                                                            </div>
                                                            <p class="text-color-white">
                                                                Soul health Care provides, besides the cure of disease, various activities and programs in prevention, rehabilitation, an d health promotion for all members of families/household.ealth begins at home. And healthy families contribute to the health of states and nations.
                                                            </p>
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="30"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="testimonial">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="188" data-mobile="150" data-smobile="110" style="height: 188px;"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="box-testimonial">
                                                    <div class="box-img-tt">
                                                        <img src="img/World-Map-Free-PNG-Image.webp" alt="image" />
                                                    </div>
                                                    <div class="box-wrap-tt text-center pdt-39 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                        <h3 class="heading">
                                                            Patient Feedback
                                                        </h3>
                                                        <div class="title-heading wow fadeInUp pdt-8" style="visibility: visible; animation-name: fadeInUp;">
                                                            What our Patient say
                                                        </div>
                                                        <div class="themesflat-spacer clearfix" data-desktop="22" data-mobile="0" data-smobile="0" style="height: 22px;"></div>
                                                        <p>
                                                            Soul Health Care Multi-specialist Hospital Patient Reviews and Testimonials
                                                        </p>
                                                    </div>
                                                    <div class="owl-carousel owl-theme none cursor-resize wow fadeInUp owl-loaded owl-drag" data-margin="30" data-tablet="2" data-mobile="1" style="visibility: visible; animation-name: fadeInUp;">
                                                        <div class="owl-stage-outer">
                                                            <div class="owl-stage" style="transform: translate3d(-1200px, 0px, 0px); transition: all 0s ease 0s; width: 3600px;">
                                                                <div class="owl-item cloned" style="width: 370px; margin-right: 30px;">
                                                                    <div class="item item-tt drop-sd">
                                                                        <div class="row-item-tt">
                                                                            <div class="img-item">
                                                                                <img src="img/testimonial01.webp" alt="image" />
                                                                            </div>
                                                                            <div class="item-name">
                                                                                <h3>Rohit Varma</h3>
                                                                                <p>Student</p>
                                                                            </div>
                                                                            <div class="box-icon font-size-quote pdt-63">
                                                                                <i class="icon-quotes-24"></i>
                                                                            </div>
                                                                        </div>
                                                                        <p class="box-post pdl-25">
                                                                            It was very positive experience since admission till discharge. Great job everyone.
                                                                        </p>
                                                                        <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="owl-item cloned" style="width: 370px; margin-right: 30px;">
                                                                    <div class="item item-tt drop-sd">
                                                                        <div class="row-item-tt">
                                                                            <div class="img-item">
                                                                                <img src="img/testimonial02.webp" alt="image" />
                                                                            </div>
                                                                            <div class="item-name">
                                                                                <h3>Puja Manhotra</h3>
                                                                                <p>Teacher</p>
                                                                            </div>
                                                                            <div class="box-icon font-size-quote pdt-63">
                                                                                <i class="icon-quotes-24"></i>
                                                                            </div>
                                                                        </div>
                                                                        <p class="box-post">
                                                                            Service and treatment is very good.also doctor was good. Thank you for taking care of my father.<br/>
                                                                        </p>
                                                                        <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="owl-item active" style="width: 370px; margin-right: 30px;">
                                                                    <div class="item item-tt drop-sd">
                                                                        <div class="row-item-tt">
                                                                            <div class="img-item">
                                                                                <img src="img/testimonial01.webp" alt="image" />
                                                                            </div>
                                                                            <div class="item-name">
                                                                                <h3>Ravi Shah</h3>
                                                                                <p>Developer</p>
                                                                            </div>
                                                                            <div class="box-icon font-size-quote pdt-63">
                                                                                <i class="icon-quotes-24"></i>
                                                                            </div>
                                                                        </div>
                                                                        <p class="box-post pdl-25">
                                                                            Good maintainence of rooms. Nice geusture by nursing staff. Overall had a very good experience and feel at home.
                                                                        </p>
                                                                        <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="owl-item active" style="width: 370px; margin-right: 30px;">
                                                                    <div class="item item-tt drop-sd">
                                                                        <div class="row-item-tt">
                                                                            <div class="img-item">
                                                                                <img src="img/testimonial02.webp" alt="image" />
                                                                            </div>
                                                                            <div class="item-name">
                                                                                <h3>Payal Dev</h3>
                                                                                <p>Bank Manager</p>
                                                                            </div>
                                                                            <div class="box-icon font-size-quote pdt-63">
                                                                                <i class="icon-quotes-24"></i>
                                                                            </div>
                                                                        </div>
                                                                        <p class="box-post">
                                                                            Hospital’s services are very good. Doctors are regularly available. Hospital is maintained very neatly.
                                                                        </p>
                                                                        <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="owl-item active" style="width: 370px; margin-right: 30px;">
                                                                    <div class="item item-tt drop-sd">
                                                                        <div class="row-item-tt">
                                                                            <div class="img-item">
                                                                                <img src="img/testimonial03.webp" alt="image" />
                                                                            </div>
                                                                            <div class="item-name">
                                                                                <h3>Chirag Das</h3>
                                                                                <p>Accountant</p>
                                                                            </div>
                                                                            <div class="box-icon font-size-quote pdt-63">
                                                                                <i class="icon-quotes-24"></i>
                                                                            </div>
                                                                        </div>
                                                                        <p class="box-post pdl-45">
                                                                            Everything is fine found to be satisfactory.Paramedical staffare are giving most attention to patients in ICU
                                                                        </p>
                                                                        <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-nav disabled">
                                                            <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                                                        </div>
                                                        <div class="owl-dots disabled">
                                                            <button role="button" class="owl-dot active"><span></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </section>
                                <div class="row">
                                    <div class="themesflat-spacer clearfix" data-desktop="290" data-mobile="160" data-smobile="70" style="height: 290px;"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'include/footer.php';?>

        </div>
    </div>

    <?php include 'include/js.php';?>

</body>
</html>
