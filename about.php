<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'include/meta.php';?>

    <title>About</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <?php include 'include/css.php';?>
</head>
<body class="counter-scroll header_sticky">
    <div class="wrapper">
        <div id="page">
            <?php include 'include/header.php';?>

            <div id="header-baner">
                <div class="container">
                    <div class="text-banner wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                        <div class="baner-tittle">
                            About Us
                        </div>
                    </div>
                </div>
            </div>

            <div id="main-content" class="site-main clearfix">
                <div id="content-wrap">
                    <div id="site-content" class="site-content clearfix">
                        <div id="inner-content" class="inner-content-wrap">
                            <div class="page-content">
                                <section class="about-us">
                                    <div class="container">
                                        <div class="row">
                                            <div class="themesflat-spacer clearfix" data-desktop="168" data-mobile="70" data-smobile="70" style="height: 168px;"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="feature-medal wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                    <img src="img/about-us.webp" alt="image" />
                                                    <div class="medal">
                                                        <div class="box-icon">
                                                            <i class="icon-medal"></i>
                                                        </div>
                                                        <div class="content-medal">
                                                            <div class="themesflat-spacer clearfix" data-desktop="60" data-mobile="60" data-smobile="60" style="height: 60px;"></div>
                                                            <h2 class="title-ab text-color-title-sidebar">Best Awarded Hospital</h2>
                                                            <div class="themesflat-spacer clearfix" data-desktop="10" data-mobile="10" data-smobile="10" style="height: 10px;"></div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="feature-medal-inner">
                                                        <img src="img/Group7027.webp" alt="image" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="box-wrap-ab wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                                    <div class="themesflat-spacer clearfix" data-desktop="20" data-mobile="50" data-smobile="50" style="height: 20px;"></div>
                                                    <h3 class="heading">
                                                        Multi Speciality Hospital
                                                    </h3>
                                                    <div class="title-heading pdbt-30">
                                                        Soul Health Care For<br />
                                                        Your Family
                                                    </div>
                                                    <p>
                                                        A 1-years old legacy in extending excellent healthcare services to the community, Soul Health Care Hospital is a leading Multispecialty Hospital based in Bhavnagar. With 27 specialty services, modern facilities, and state-of-art infrastructure, we constantly strive to lend the most advanced healthcare services and accessible & affordable treatment that ensure smiles.
                                                    </p>
                                                    <div class="themesflat-spacer clearfix" data-desktop="20" data-mobile="20" data-smobile="20" style="height: 20px;"></div>
                                                    <ul class="pdt-15">
                                                        <li>
                                                            <a href="services.html"><i class="check-icon"></i> Critical Care</a>
                                                        </li>
                                                        <li class="mgr-none">
                                                            <a href="services.html"> <i class="check-icon"></i>OPD Facilities</a>
                                                        </li>
                                                        <li>
                                                            <a href="services.html"><i class="check-icon"></i>Operative Department</a>
                                                        </li>
                                                        <li class="mgr-none">
                                                            <a href="services.html"> <i class="check-icon"></i> In-built Medical Store</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="70" data-smobile="70" style="height: 120px;"></div>
                                <div class="themesflat-spacer clearfix" data-desktop="120" data-mobile="70" data-smobile="70" style="height: 120px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'include/footer.php';?>
        </div>
    </div>

<?php include 'include/js.php';?>
</body>
</html>
